package com.yelbota.cgml.mavenPlugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @goal as3
 * @phase generate-sources
 * @requiresDependencyResolution compile
 */
@SuppressWarnings("UnusedDeclaration")
public class AS3CgmlMojo extends AbstractCgmlcMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        execute(OutputType.AS3);
    }
}
