/*
 * This file is part of the CGML Compiler
 *
 * Copyright(c) 2010 Aleksey Fomkin <aleksey.fomkin@gmail.com>
 * http://bitbucket.org/yelbota/cgml-compiler
 *
 * This file may be licensed under the terms of of the
 * GNU General Public License Version 3 (the ``GPL'').
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the GPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the GPL along with this
 * program. If not, go to http://www.gnu.org/licenses/gpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

package com.yelbota.cgml.mavenPlugin;

import com.yelbota.cgml.mavenPlugin.utils.CleanStream;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

abstract class AbstractCgmlcMojo extends AbstractMojo {

    public enum OutputType {

        JAVA("-java"),
        HAXE("-haxe"),
        AS3("-as3");

        private String type;

        @Override
        public String toString() {
            return type;
        }

        OutputType(String s) {
            type = s;
        }
    }

    /**
     * @parameter expression="${plugin.artifacts}"
     * @readonly
     * @required
     */
    protected List<Artifact> pluginArtifacts;

    /**
     * The Maven project.
     *
     * @parameter default-value="null" expression="${project}"
     */
    protected MavenProject project;

    /**
     * @parameter default-value="true"
     */
    protected boolean javaUseDouble;

    /**
     * @parameter
     */
    protected List<String> define;

    /**
     * @parameter
     * @required
     */
    @SuppressWarnings({"UnusedDeclaration"})
    protected File sourceDirectory;

    /**
     * @parameter default-value="${project.build.directory}/generated-sources/cgml"
     * @required
     */
    @SuppressWarnings({"UnusedDeclaration"})
    private File outputDirectory;

    /**
     * @parameter
     */
    @SuppressWarnings({"UnusedDeclaration"})
    protected String modelPrefix;

    protected void execute(OutputType type) throws MojoExecutionException, MojoFailureException {

        List<String> cmd = new ArrayList<String>();

        if (!outputDirectory.exists())
            outputDirectory.mkdirs();

        cmd.add("-source");
        cmd.add(sourceDirectory.getAbsolutePath());
        cmd.add(type.toString());
        cmd.add(outputDirectory.getAbsolutePath());

        if (modelPrefix != null) {
            cmd.add("--model-prefix");
            cmd.add(modelPrefix);
        }

        if (javaUseDouble) {
            cmd.add("--java-use-double");
            cmd.add(modelPrefix);
        }

        if (define != null) {
            for (String d : define) {
                cmd.add("--define");
                cmd.add(d);
            }
        }

        File artifactFile = null;
        if (pluginArtifacts != null) {

            // Lookup plugin artifacts.
            for (Artifact pluginArtifact : pluginArtifacts) {

                boolean eqGroupId = pluginArtifact.getGroupId().equals("com.yelbota.cgml");
                boolean eqArtifactId = pluginArtifact.getArtifactId().equals("cgml-compiler");

                if (eqGroupId && eqArtifactId) {
                    artifactFile = pluginArtifact.getFile();
                    break;
                }
            }
        }

        if (artifactFile != null) {
            executeCmd(artifactFile, cmd);
            project.addCompileSourceRoot(outputDirectory.getAbsolutePath());
        }
        else {
            throw new MojoFailureException("No cgml-compiler plugin dependency found.");
        }
    }

    private void executeCmd(File artifactFile, List<String> args) throws MojoFailureException {


        try {

            List<String> cmd = new ArrayList<String>();

            cmd.add("java");
            cmd.add("-jar");
            cmd.add(artifactFile.getAbsolutePath());
            cmd.addAll(args);

            String[] cmdArray = cmd.toArray(new String[cmd.size()]);
            getLog().debug(StringUtils.join(cmdArray, " "));

            Process process = Runtime.getRuntime().exec(cmdArray);
            CleanStream cleanError = new CleanStream(process.getErrorStream(), getLog(), CleanStream.CleanStreamType.ERROR);
            CleanStream cleanOutput = new CleanStream(process.getInputStream(), getLog(), CleanStream.CleanStreamType.INFO);

            cleanError.start();
            cleanOutput.start();

            int code = process.waitFor();

            if (code > 0) {
                throw new MojoFailureException("cgmlc process finished with exit code #" + code);
            }

        } catch (IOException e) {
            throw new MojoFailureException("i/o error: " + e.getMessage());
        } catch (InterruptedException e) {
            throw new MojoFailureException("cgml process was interrupted");
        }
    }
}
