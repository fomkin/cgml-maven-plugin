/*
 * This file is part of the CGML Compiler
 *
 * Copyright(c) 2010 Aleksey Fomkin <aleksey.fomkin@gmail.com>
 * http://bitbucket.org/yelbota/cgml-compiler
 *
 * This file may be licensed under the terms of of the
 * GNU General Public License Version 3 (the ``GPL'').
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the GPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the GPL along with this
 * program. If not, go to http://www.gnu.org/licenses/gpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 */

package com.yelbota.cgml.mavenPlugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @goal java
 * @phase generate-sources
 * @requiresDependencyResolution compile
 */
@SuppressWarnings({"UnusedDeclaration"})
public class JavaCgmlMojo extends AbstractCgmlcMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        execute(OutputType.JAVA);
    }
}
