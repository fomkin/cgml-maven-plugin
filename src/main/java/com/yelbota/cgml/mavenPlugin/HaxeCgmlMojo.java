package com.yelbota.cgml.mavenPlugin;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * @goal haxe
 * @phase generate-sources
 * @requiresDependencyResolution compile
 */
@SuppressWarnings({"UnusedDeclaration"})
public class HaxeCgmlMojo extends AbstractCgmlcMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        execute(OutputType.HAXE);
    }
}
